<?php
$value=$_POST['admin'];
if($value!='admin'){
header('location:index.php');
}
?>
<?php
include 'nav.php';
?>
<!-- get all the values from database to be inserted into the fields -->
<?php
include 'connect.php';
$query=mysql_query("SELECT * FROM general");
	$numrows=mysql_num_rows($query);
	if($numrows!=0)
	{
	while($row=mysql_fetch_assoc($query))
		{
	$dbattnmsg=$row['attnmsg'];
	$dbmonth=$row['month'];
	}
	}
	else{
	$dbattnmsg=NULL;
	$dbmonth=NULL;
	}
	?>
	
<html>
<head>
<script type="text/javascript" src="http://code.jquery.com/jquery-1.4.2.min.js"></script>
	<script type="text/javascript" src="http://ajax.microsoft.com/ajax/jquery.validate/1.7/jquery.validate.min.js"></script>
	<script type="text/javascript">
	$(document).ready(function(){
		$("#myform").validate({
			debug: false,
			rules: {
				name: "required",
				amount: {
					required: false,
					email: false
				}
			},
			messages: {
				name: "Please let us know who you are.",
				email: "A valid email will help us get in touch with you.", 
			},
			submitHandler: function(form) {
				// do other stuff for a valid form
				$.post('adminprocess.php', $("#myform").serialize(), function(data) {
					$('#results').html(data);
				});
			}
		});
	});
	</script>
	<!-- <style>
	label.error { width: 250px; display: inline; color: red;}
	</style>-->
	<title>myExpenses | Admin Panel</title> 
	<style type="text/css">
body{
margin-top: 150px;
font-family:"Lucida Grande", "Lucida Sans Unicode", Verdana, Arial, Helvetica, sans-serif;
font-size:12px;
}
p, h1, form, button{border:0; margin:0; padding:0;}
.spacer{clear:both; height:1px;}
/* ----------- My Form ----------- */
.myform{
margin:0 auto;
width:400px;
padding:50px;
}

/* ----------- stylized ----------- */
#stylized{
border:solid 2px #b7ddf2;
background:#ebf4fb;
}
#stylized h1 {
font-size:14px;
font-weight:bold;
margin-bottom:8px;
}
#stylized p{
font-size:11px;
color:#666666;
margin-bottom:20px;
border-bottom:solid 1px #b7ddf2;
padding-bottom:10px;
}
#line{
font-size:11px;
color:#666666;
margin-bottom:20px;
border-bottom:solid 1px #b7ddf2;
padding-bottom:10px;
}
#stylized label{
display:block;
font-weight:bold;
text-align:right;
width:140px;
float:left;
}
#stylized .small{
color:#666666;
display:block;
font-size:11px;
font-weight:normal;
text-align:right;
width:140px;
}
#stylized input{
float:left;
font-size:12px;
padding:4px 2px;
border:solid 1px #aacfe4;
width:200px;
margin:2px 0 20px 10px;
}
td{
padding-top: 15px;
padding-bottom:0px;
border:1px #B7DDF2;
border-style: dotted;
border-width:2px;
font-size:12px;
}
#stylized button{
clear:both;
margin-left:150px;
width:125px;
height:31px;
background:#666666 url(img/button.png) no-repeat;
text-align:center;
line-height:31px;
color:#FFFFFF;
font-size:11px;
font-weight:bold;
}
</style>
	
</head>
<body>







<!-- This forms is for creating new backup of the current report from cache -->
<div id="stylized" class="myform">
<form name="mynewform" id="mynewform" action="backitup.php" method="POST"> 
<h1>Backup current month</h1>
<p>Please fill in all details</p>
<div id="results"></div>

<label>Insert valid file name
<span class="small">will save as .html</span>
</label>
<input type="text" name="month_name" id="month_name" value="" />
<!-- The Submit button -->
	<button type="submit">Create Backup</button>
</form><br />

<form name="mynewform1" id="mynewform1" action="backitup.php" method="POST"> 
<h1>Clear the Expenses Table</h1>
<p>Did you run backup first?</p>
<div id="results"></div>
<!-- The Submit button -->
	<button type="submit">Clear Expenses</button>
</form><br />
<script type="text/javascript">
	$(document).ready(function(){
		$("#mynewform1").validate({
			debug: false,
			rules: {
				name: "required",
				amount: {
					required: false,
					email: false
				}
			},
			messages: {
				name: "Please let us know who you are.",
				email: "A valid email will help us get in touch with you.", 
			},
			submitHandler: function(form) {
				// do other stuff for a valid form
				$.post('truncate.php', $("#mynewform1").serialize(), function(data) {
					$('#results').html(data);
				});
			}
		});
	});
	</script>



<p>
<form name="myform" id="myform" action="" method="POST"> 
<h1>Administration Panel</h1>
<p>Please fill in all details</p>
<div id="results"></div>
<!-- for attention message -->
<label>Attention Message
<span class="small">Flag Message</span>
</label>
<input type="text" name="attnmsg" id="attnmsg" value="<?php echo($dbattnmsg); ?>" />

<!-- for month label -->
<label>Month Label
<span class="small">Month of Expense</span>
</label>
<input type="text" name="month" id="month" value="<?php echo($dbmonth); ?>" />


<!-- for deleting a record-->
<label>Delete a Record
<span class="small">Enter record id</span>
</label>
<input type="text" name="delid" id="delid" />
<!-- change password -->
<!-- set privilege -->

<table><td>
<h1 style="font-size:12px;">New Password & Privilege</h1>
<p>Change password and add admin privileges</p>
<label>Username
<span class="small">Input username</span>
</label>
<input type="text" name="client_username" id="client_username" value="" />
<label>New Password
<span class="small">Input new Password</span>
</label>
<input type="text" name="client_password" id="client_password" value="" />
<label>Set admin privilege
<span class="small">Type "admin" w/o quotes</span>
</label>
<input type="text" name="client_level" id="client_level" value="<?php echo('none'); ?>" />
</td></table>
<br>
<div class="spacer"></div>

<!-- The Submit button -->
	<button type="submit">Save All</button>
</form>
<!-- We will output the results from process.php here -->
<div id="results"><div>
 <div class="spacer"></div> 
<a href="reports.php">View Full Report</a><br>
  <a href="userpurchase.php" >View Individual Purchase Report</a>
</div>

</html>