<?php
if (!session_id())
    session_start();
if (!$_SESSION['logon']) {
    header("Location:index.php");
    die();
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<html xmlns="http://www.w3.org/1999/xhtml"> 
    <head> 
        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js"></script>

        <link rel="stylesheet" type="text/css" href="js/jkmegamenu.css" />

        <script type="text/javascript" src="js/jkmegamenu.js">

            /***********************************************
             * jQuery Mega Menu- by JavaScript Kit (www.javascriptkit.com)
             * This notice must stay intact for usage
             * Visit JavaScript Kit at http://www.javascriptkit.com/ for full source code
             ***********************************************/

        </script>

        <script type="text/javascript">

            //jkmegamenu.definemenu("anchorid", "menuid", "mouseover|click")
            jkmegamenu.definemenu("megaanchor", "megamenu1", "mouseover")

        </script>
        <script type="text/javascript" src="http://code.jquery.com/jquery-1.4.2.min.js"></script>
        <script type="text/javascript" src="http://ajax.microsoft.com/ajax/jquery.validate/1.7/jquery.validate.min.js"></script>
        <script type="text/javascript">
            $(document).ready(function(){
                $("#notesform").validate({
                    debug: false,
                    submitHandler: function(form) {
                        // do other stuff for a valid form
                        $.post('notesupdate.php', $("#notesform").serialize(), function(data) {
                            $('#notesresults').html(data);
                        });
                    }
                });
            });
        </script>
        <title>myExpenses</title> 
        <?php
        include 'connect.php';
        $username = $_SESSION['first_name'];
        $query = mysql_query(" SELECT * FROM users WHERE username='$username' ");
        $numrows = mysql_num_rows($query);
        if ($numrows != 0) {
            while ($row = mysql_fetch_assoc($query)) {
                $dbnotes = $row['notes'];
            }
        } else {
            $dbnotes = "Could not connect to the database";
        }
        ?>

        
        <style type="text/css"> 
            <!-- 

            #foo{
                top: 40;
                right:0;
                width: 300px;
                height: 260px;
                position: fixed;
                border:solid 1px #b7ddf2;
                padding: 10px;
                background:#ebf4fb;

            }
            #megamenu1{
                position: fixed;
                left: 0;
                top: 0;
                display: none;

                background: white;
                border: 1px solid #95FF4F;
                border-width: 5px 1px;
                padding: 10px;
                font: normal 12px Verdana;
                z-index: 100;

            }
            #megaanchor{
                top: 0;
                margin: 0 auto;
                position: absolute;
                padding: 10px;   
                font-weight: bold;
                background-color: #73d550;
                color: #2e2d21;
                z-index: 100;
            }


            a { color:#007ca5; text-decoration:none; outline: none; }

            a:visited { color:#666; }

            a:active { color:#999; }

            #text {
                position: absolute;
                top: 0;
                right: 0;
                margin: 0 auto;
                height: 20px;
                padding: 10px;
                text-align: right;
                font-weight: bold;
                color: #802A2A;
            }

            #navbar ul { 
                position: absolute;
                top: 0;
                left: 0;
                right: 0;
                margin: 0 auto;
                height: 20px;
                width: auto;
                padding: 10px;

                list-style-type: none; 
                text-align: center; 
                background-color: #95FF4F; 
            } 

            #navbar ul li {  
                display: inline; 
            } 

            #navbar ul li a { 
                text-decoration: none; 
                padding: .2em 1em; 
                color: #000; 
                background-color: #95FF4F; 
            } 

            #navbar ul li a:hover { 
                color: #000; 
                background-color: #fff; 
            } 

            --> 
        </style> 
    </head> 
    <body> 

        <!--Mega Menu Anchor-->
        <a href="#" id="megaanchor">My Notes</a>




        <!--Mega Drop Down Menu HTML. Retain given CSS classes-->
        <div id="megamenu1" class="megamenu">

            <div class="column">
                <form id="notesform">
                    <TEXTAREA id="notes" style="resize: none;" NAME="notes" value="" COLS=38 ROWS=15><?php echo ($dbnotes); ?></TEXTAREA>
                    <br/>
                    <input type="submit" value="Save" /><br><br><div id="notesresults"></div>
                </form>
            </div>



        </div>

        <div id="navbar"> 
            <ul> 
                <li><a href="/myExpenses/purchase.php">Add Purchase</a></li> 
                <li><a href="/myExpenses/reports.php">View Report</a></li> 
                <li><a href="/myExpenses/backup.php">View Backup</a></li> 
                

<?php
$username = $_SESSION['first_name'];
echo "<p id='text'>" . $username . " |<a href='logout.php'> log out</a></p>";
?>
            </ul> 

        </div> 
        </div>

        </div>
    </body> 
</html>