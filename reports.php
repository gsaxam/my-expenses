<?php
include 'nav.php';
?>
<head>
<link href="tablecloth/tablecloth.css" rel="stylesheet" type="text/css" media="screen" />
<script type="text/javascript" src="tablecloth/tablecloth.js"></script>
<meta charset="utf-8" />
<title>myExpenses | Reports</title>
<style type="text/css">
* { margin: 0; padding: 0; }
html { height:100%; }
#label{
font: bold 14px "Verdana", "MS Serif", "New York", serif;
margin: 0;
padding: 0;
color:  #426352;
border-top: solid #7F9A65 medium;
border-bottom: dotted #66CCCC thin;
width: auto;
}
#leftdata{
float: left;
text-align: left;
color: #5D478B;
}
#rightdata{
float: right;
text-align: right;
color: #458B00;
}
hr{
border: 1px dashed #B0E0E6;
}
body {
text-align: left;
font-size: 10pt;
width: 100%;
height: 100%;
font-size: 12px;
font-family: Lucida Grande, Lucida Sans Unicode, Verdana, Arial, Helvetica, sans-serif;
color: #000;
background: #fff;
margin: 0;
border: 0;
padding: 0; }
a { color:#007ca5; text-decoration:none; outline: none; }
a:visited { color:#666; }
a:active { color:#999; }
.left_col a { color:#fff; text-decoration: none; outline: none; }
.left_col a:hover { color:#fff; text-decoration: underline; outline: none; }
.left_col a:visited { color:#fff; }
.left_col a:active { color:#fff; }
img, a img { border-style: none; }
h3, .h3 { font-size: 1.8em; } /* 18pt */
.small { font-size: 1em; }
.clearer { clear:both; }
.header {
text-align: left;
background: #eceae1;
color: #328AA4;
width: 980px;
margin: 40px 0 0 0;
border-width: 1px;
border-color:#000000;
padding: 10px;
overflow: hidden;
}
.left_col {
text-align: left;
float: left;
background: #ECFBD4;
font-family: Verdana, Arial, Helvetica, sans-serif;
color: #fff;
width: auto;
height: auto;
margin: 0 0 0 0;
border-width: 5px;
border-color:black;
padding: 30px;
overflow: hidden;
}
.main_col {
text-align: left;
float: right;
background: #fff;
color: #000;
width: 680px;
height: 100%;
margin: 0 0 0 0;
border: 0;
padding: 0px;
overflow: hidden;
}
.footer {
text-align: left;
background: #eceae1;
color: #000;
width: 980px;
margin: 0 0 0 0;
border: 0;
padding: 10px;
overflow: hidden;
}
.wrapper {
text-align: center;
width: 1000px;
margin-left: auto;
margin-right: auto;
}
</style>
</head>
<body>
<?php
include 'connect.php';
$query=mysql_query("SELECT * FROM general");
$numrows=mysql_num_rows($query);
if($numrows!=0)
{
// fetch all rows that are available into variable $row
while($row=mysql_fetch_assoc($query))
{
$dbattnmsg=$row['attnmsg'];
$dbmonth=$row['month'];
}
}
?>
<div class="wrapper">
<div class="header" style='border: 1px #b1bc9f solid;'>
<br />
<h3>All Users Report for <?php echo($dbmonth); ?></h3>
<br/><br/>
</div>
<div class="main_col">
<table cellspacing="0" cellpadding="0">
<tr>
<th><font face="Verdana" size=2>ID</font></th>
<th><font face="Verdana" size=2>Name</font></th>
<th><font face="Verdana" size=2>Store</font></th>
<th><font face="Verdana" size=2>Date</font></th>
<th><font face="Verdana" size=2>Amount</font></th>
<th><font face="Verdana" size=2>Remarks</font></th>
</tr>
<?php
include 'connect.php';
// get all users..
$query=mysql_query(" SELECT * FROM users");
$numrows=mysql_num_rows($query);
$user = array();
$i=0; // counter for retrieving usernames.. since for loop cannot be used
if($numrows!=0)
{
// fetch all rows that are available into variable $row and extract username and password from the database
while($row=mysql_fetch_assoc($query))
{
$user[$i]=$row['username']; // get all users from the users table and assign it into arrays.
$i++;
}
}
// create a report
$usertotal=array();
for($a=0;$a<count($user);$a++){
$usertotal[$a]=0;
}
$query=mysql_query(" SELECT * FROM expenses");
$numrows=mysql_num_rows($query);
if($numrows!=0)
{
// fetch all rows that are available into variable $row and extract username and password from the database
while($row=mysql_fetch_assoc($query))
{
echo("<tr><center><td bgcolor=#F8F8FF><font color='black' size=2>".$row['items']."</td>");
echo("<td><center><font color='darkblue' size=2>".$row['name']."</td>");
echo("<td><center><font color='darkred' size=2>".$row['merchant']."</td>");
echo("<td><center><font color='black' size=2>".$row['timestamp']."</td>");
echo("<td><center><font color='forestgreen' size=2>".number_format($row['amount'],2)."</td>");
echo("<td><center><font color='blue' size=2>".$row['remarks']."</td>");
for($j=0;$j<count($user);$j++){
if($user[$j]==$row['name']) {
//echo("This is the result ".$user[$j]);
$usertotal[$j]=($usertotal[$j])+($row['amount']);
//echo("This is the result ".$usertotal[$j]);
}
//echo("$total ");
}
}
}
echo("</table></div>");
$total1=0;
$query=mysql_query(" SELECT * FROM expenses WHERE amount>0");
$numrows=mysql_num_rows($query);
if($numrows!=0)
{
// fetch all rows that are available into variable $row and extract username and password from the database
while($row=mysql_fetch_assoc($query))
{
$total1+=$row['amount'];
}
//echo("<font color='red' size=3>Expenditure: ".$total1);
}
echo("<div class='left_col' style='border: 1px #b1bc9f solid;'><b><div id='label'>Expenses </div></b><br>");
echo("<b><font color='#B87333' size=2>Total: $".number_format($total1,2)."</font><br><br></b>");
echo("<b><font color='#708090' size=2>Average: $".number_format($total1/count($user),2)."</font><br><br></b>");
for($k=0;$k<count($user);$k++){
echo("<div id='leftdata'>".$user[$k].":</div><div id='rightdata'>$".number_format($usertotal[$k],2)."</div><br><hr>");
}
echo("<br/><br/>");
echo "<div id='label'>Comparisions</div><br/>";
$total = 0;
$i = 0;
foreach($usertotal as $k=>$v){
$total += $v;
$i++;
}
$average = $total/$i;
$payA = array();
$recA = array();
foreach($usertotal as $k=>$v){
$topay = $average-$v;
if($topay == 0){
echo "<div id='leftdata'>".$user[$k].":</div><div id='rightdata'>".$topay."</div><br/><hr>\r\n";
}elseif($topay >0){
echo "<div id='leftdata'>".$user[$k].":</div><div id='rightdata'> -$".$topay."</div><br/><hr>\r\n";
$payA[$k] = $topay;
}else{
echo "<div id='leftdata'>".$user[$k].":</div><div id='rightdata'> +$".abs($topay)."</div><br/><hr>\r\n";
$recA[$k] = abs($topay);
}
}
echo "<br />";
echo "<div id='label'>Transactions</div><br/>";
arsort($recA);
arsort($payA);
foreach($payA as $k=>$v){
arsort($recA); //after first person paid re-sort array so person who needs to receive most at top
foreach($recA as $key=>$value){
echo"<font color='#2F4F4F'>";
if($v==0){
// do nothing
}
elseif($v > $value){
echo $user[$k]."<font color='#6F7285'> pays back to </font>".$user[$key]." <font color='#458B00'>$".number_format($value,2)."</font><hr><br/>\r\n";
$v = $v - $value;
$payA[$k]=$payA[$k]-$value;
$recA[$key] = 0;
}elseif($v < $value){
echo $user[$k]."<font color='#6F7285'> pays back to </font>".$user[$key]." <font color='#458B00'>$".number_format($v,2)."</font><hr><br/>\r\n";
$recA[$key] = $value - $v;
$payA[$k] = 0;
$v=0; // tell current loop this person doesn't owe anymore
}else{
echo $user[$k]."<font color='#6F7285'> pays back to </font>".$user[$key]." <font color='#458B00'>$".number_format($value,2)."</font><hr><br/>\r\n";
$recA[$key] = 0;
$payA[$k] = 0;
$v=0;
}
}
}
echo"</font>";
?>
</span></h2>
<br class="clearer" /"></div><div class="footer">
<span class="small">
Generated by myExpenses
<br />
Not designed for Internet Explorer
</span>
</div>
</body>
</html>