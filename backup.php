<?php
if (!session_id()) session_start();
if (!$_SESSION['logon']){ 
    header("Location:index.php");
    die();
}
?>
<?php
include 'nav.php';
?>
	
<html>
<head>

	<!-- <style>
	label.error { width: 250px; display: inline; color: red;}
	</style>-->
	<title>myExpenses | Admin Panel</title> 
	<style type="text/css">
body{
margin-top: 150px;
font-family:"Lucida Grande", "Lucida Sans Unicode", Verdana, Arial, Helvetica, sans-serif;
font-size:12px;
}
p, h1, form, button{border:0; margin:0; padding:0;}
.spacer{clear:both; height:1px;}
/* ----------- My Form ----------- */
.myform{
margin:0 auto;
width:400px;
padding:50px;
}

/* ----------- stylized ----------- */
#stylized{
border:solid 2px #b7ddf2;
background:#ebf4fb;
}
#stylized h1 {
font-size:14px;
font-weight:bold;
margin-bottom:8px;
}
#stylized p{
font-size:11px;
color:#666666;
margin-bottom:20px;
border-bottom:solid 1px #b7ddf2;
padding-bottom:10px;
}
#line{
font-size:11px;
color:#666666;
margin-bottom:20px;
border-bottom:solid 1px #b7ddf2;
padding-bottom:10px;
}
#stylized label{
display:block;
font-weight:bold;
text-align:right;
width:140px;
float:left;
}
#stylized .small{
color:#666666;
display:block;
font-size:11px;
font-weight:normal;
text-align:right;
width:140px;
}
#stylized input{
float:left;
font-size:12px;
padding:4px 2px;
border:solid 1px #aacfe4;
width:200px;
margin:2px 0 20px 10px;
}
td{
padding-top: 15px;
padding-bottom:0px;
border:1px #B7DDF2;
border-style: dotted;
border-width:2px;
font-size:12px;
}
#stylized button{
clear:both;
margin-left:150px;
width:125px;
height:31px;
background:#666666 url(img/button.png) no-repeat;
text-align:center;
line-height:31px;
color:#FFFFFF;
font-size:11px;
font-weight:bold;
}
</style>
	
</head>
<body>


<div id="stylized" class="myform">
<div id="results"></div>

<h1>Backed Up Files</h1>
<p>Showing all previous month transactions</p>
<!--<div id="results"></div>-->

<!-- get all the values from database to be inserted into the fields -->
<?php
include 'connect.php';
$query=mysql_query("SELECT * FROM backup");
	$numrows=mysql_num_rows($query);
	
	if($numrows!=0)
{
// fetch all rows that are available into variable $row 
while($row=mysql_fetch_assoc($query))
{
echo("<table width=400 style='table-layout:fixed'><col width=10><col width=50><col width=20><tr><td bgcolor=#F8F8FF><font color='black' size=2>".$row['id']."</td>");
echo("<td><center><font color='darkblue' size=2><a href='".$row['url']."'>".$row['filename']."</a></td>");
echo("<td><center><font color='black' size=2>".$row['timestamp']."</td></table>");

	}
	}
	
	?>


 <div class="spacer"></div> 
</div>

</html>