<?php
## Written and provided to you by Veedeoo or PoorBoy 2012
## this class has no warranty of any kind.
## feel free to use this to your application as needed
## feel free to extend this class for improved and better functionalities.
## What I meant by improved and better functionalities? Function like compression of cache file e.g. gzip.
    class MakeCache{
        private $content= null;
        private $cacheDuration = null;
        private $cacheLoc = null;
        private $cacheFile = null;
        public function __construct($content,$cacheDuration,$cacheLoc,$cacheFile){
         $this->loc = $cacheLoc;
         $this->file = $cacheFile;
         $this->duration = $cacheDuration;
         //$this->comp = $cacheCompression;
         $this->content = $content;
        }
        private function checkDuration(){
            return(file_exists($this->loc."/".$this->file) && (time() - $this->duration < filemtime($this->loc."/".$this->file))? true : false); 
        }
        public function useCache(){
         if(self::checkDuration()){
             ob_start( );
        include( $this->loc."/".$this->file);
        $this->output = ob_get_clean( );
        }
            else{
                ob_start(); 
        ## include the page to be cached
        include ($this->content);
        ## prepare for writing the cache file
        $filePointer = fopen($this->loc."/".$this->file, 'w');
        ## grab the output buffer and write it in cachefile
        fwrite($filePointer, ob_get_contents());
        ## close the file
        fclose($filePointer);
        ## Send the output to the browser
         $this->output = ob_end_flush();
            }
            return $this->output;
        }
    }
    ## end of class
?>